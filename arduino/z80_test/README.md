# Class-Based Example

This example demonstrates using a class-based approach to driving the Z80 processor, where all the logic of memory and port-based I/O is handled via a simple class, and you place your project-specific logic in your sketch.

## z80_test.ino

This example instantiates an instance of the class, and then configures the several callbacks:

* A callback executed when a byte is fetched from memory.
  * This returns memory from our hardwired-program included inline.
  * The source of that program is [z80_test.z80](z80_test.z80).
* A callback executed when a byte is written to an I/O port.
  * If the byte is written to address `1` it is output to the serial-console.
* A callback executed when a byte is requested from an I/O port.
  * If the address is `1` the latest byte from the Arduino serial-monitor is returned.

All together this allows a simple program to be executed upon the Z80 processor which will endlessly prompt for input from the serial-console, and output it after converting anything appropriate to upper-case.

## Further Examples

The Z80RetroShield class is [maintained elsewhere](https://github.com/skx/z80retroshield/), but is included here to make the example 100% standalone.

If you wish you can install the Z80RetroShield library via the Arduino library-manager, where you'll find several example programs.
