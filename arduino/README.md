# RetroShield Z80 for Arduino Mega

This is the software project folder for the RetroShield Z80.

There are three examples supplied:

* [kz80_efex](kz80_efex)
  * Efex is a monitor with inline assembler and disassembler.
* [kz80_test](kz80_test)
  * An example of interfacing with an LCD/keypad an the Z80.
* [z80_test](z80_test)
  * A stand-alone example showing a class-based approach to driving the Z80.

* `kz80_test`: bring-up & test work.
* `kz80_grantz80`: Microsoft Basic v4.7 modified by Grant Searle
* `kz80_efex`: Efex Monitor by Mustafa Peker.
